import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    # TODO: has to be replaced in production
    SECRET_KEY = 'O987ZIU8uhzsdk875kjjqpoyme'
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = \
        'sqlite:///' + os.path.join(basedir, 'flask-data.sqlite')

config = {
    'development': DevelopmentConfig,

    'default': DevelopmentConfig
}
