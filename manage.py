#!/usr/bin/python3

import os
from app import create_app, db
from app.models import Card
from flask.ext.script import Manager, Shell

app = create_app(os.getenv('FLASK_CONFIG') or 'default')

manager = Manager(app)


def make_shell_context():
    return dict(app=app, db=db, Card=Card)

manager.add_command('shell', Shell(make_context=make_shell_context))

#db.create_all()

if __name__ == '__main__':
    manager.run()
