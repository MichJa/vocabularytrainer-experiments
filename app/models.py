
import datetime
import sqlalchemy

from . import db


class Card(db.Model):
    __tablename__ = 'cards'
    id = db.Column(db.Integer, primary_key=True)
    front = db.Column(db.String(256))
    back = db.Column(db.String(256))
    stack = db.Column(db.Integer, default=-1)
    groups = db.Column(db.String(256), default="NA")
    last_correct = db.Column(
        sqlalchemy.types.TIMESTAMP,
        default = datetime.datetime.strptime("1000-01-01", "%Y-%m-%d")
    )

