
from flask import render_template, session, flash, url_for, redirect, request
from flask.ext.wtf import Form
from wtforms import StringField, SubmitField
from wtforms.validators import Required

import sqlite3
import difflib
import time, datetime

from . import main
from .. import db
from ..models import Card


@main.route('/')
def page_main():
    return render_template('main.html')


class CardInputForm(Form):
    front = StringField("front", validators=[Required()])
    back = StringField("back", validators=[Required()])
    submit = SubmitField("Submit")


@main.route('/input/', methods=['GET','POST'])
def card_input():
    form = CardInputForm()

    if form.validate_on_submit():
        db.session.add(Card(front=form.front.data, back=form.back.data))
        db.session.commit()
        flash("Card (" + form.front.data + "," + form.back.data + ") stored.")
        return redirect(url_for('.card_input'))

    return render_template('input.html', form=form)


@main.route('/list/', methods=['GET','POST'])
def card_list():
    card_id = request.form.get('delete', -1, int)

    if card_id > 0:
        db.engine.execute('DELETE FROM cards WHERE id=?', card_id)

    topic = request.args.get('topic', None)

    if topic is not None:
        cards = Card.query.filter(Card.groups == topic).all()
    else:
        cards = Card.query.all()

    return render_template('list.html', cards=cards)


@main.route('/topics/', methods=['GET'])
def topics_list():

    learn_topic = request.args.get('learn', None)
    list_topic = request.args.get('list', None)

    if learn_topic is not None:
        return redirect(url_for('.learn') + "?topic=" + learn_topic)
    elif list_topic is not None:
        return redirect(url_for('.card_list') + "?topic=" + list_topic)

    received = db.engine.execute('SELECT groups, count(groups) FROM cards GROUP BY groups').fetchall()
    topics = [{'name': c[0], 'count': c[1]} for c in received]
    return render_template('topics.html', topics=topics)


def store_action(card_id, score):
    date = [(card_id, time.time(), score)]
    conn = sqlite3.connect('userhist.db')
    c = conn.cursor()
    c.executemany('INSERT INTO useraction VALUES (?,?,?)', date)
    conn.commit()
    conn.close()


def getCorrection(strA, strB, modifier, wspan):
    result = ''
    nCorrect, nWrong = 0, 0
    for i, s in enumerate(difflib.ndiff(strA, strB)):
        if s[0] == ' ':
            result = result + wspan.format(modifier='correct', word=s[-1])
            nCorrect += 1
        elif (modifier=='missing') & (s[0]=='+'):
            result = result + wspan.format(modifier=modifier, word=s[-1])
            nWrong += 1
        elif (modifier=='excessive') & (s[0]=='-'):
            result = result + wspan.format(modifier=modifier, word=s[-1])
            nWrong += 1
    return(result, nCorrect, nWrong)


def get_min_card_id():
    return db.engine.execute('SELECT MIN(id) FROM cards').fetchone()[0]


def get_cards(topic, n=10):

    if topic is None:
        cards = Card.query.filter(Card.stack >= 0).all()
    else:
        cards = Card.query.filter(Card.groups == topic).filter(Card.stack >= 0).all()

    result = []

    for card in cards:
        if card.stack == 0 or datetime.datetime.now() - card.last_correct > datetime.timedelta(days=card.stack):
            result.append(card)

    if len(result) < n:
        if topic is None:
            more_cards = Card.query.filter(Card.stack < 0).all()
        else:
            more_cards = Card.query.filter(Card.groups == topic).filter(Card.stack < 0).all()

        result.extend(more_cards)

    if result is None or len(result) < n:
        return result
    else:
        return result[0:n]


def create_card_session(topic=None):
    cards = get_cards(topic, 10)

    if len(cards) < 1:
        return None

    session['card_buffer'] = [c.id for c in cards[1:]]
    session['active_card'] = cards[0].id
    session['score'] = -1

    return cards[0]


@main.route('/learn/', methods=['GET', 'POST'])
def learn():
    
    action = request.form.get('action', "none")
    answer = request.form.get('answer', "")
    topic = request.args.get('topic', None)

    if not session.get('active_card') or topic:
        card = create_card_session(topic)
    else:
        card = Card.query.filter_by(id=session.get('active_card')).first()

    # no cards available
    if not card:
        flash("Session is complete.")
        return redirect(url_for('.page_main'))
    
    answerUser, answerSystem = '', ''
    
    wspan = '<span class="{modifier}">{word}</span>'

    # The score takes values between 0 and 1.
    # 0 means no letters are correct, and 1 means all letters are correct.
    # Formula: score = correct letters / (correct + missing + incorrect letters)
    score = 0.0
    
    if action == 'review':
        # assess action and create result parts
        if not answer:
            answer = ''
        if card.back == answer:
            answer = answer + " &#10004;"
            answerUser = wspan.format(modifier='correct', word=answer)
            score = 1.0
        else:
            answerUser, nCorrect, nWrong1 = getCorrection(answer, card.back, 'excessive', wspan)
            answerSystem, nCorrect, nWrong2 = getCorrection(answer, card.back, 'missing', wspan)
            score = nCorrect / (nCorrect + nWrong1 + nWrong2)
            store_action(card.id, score)

        if session.get('score') < 0:
            session['score'] = score

            old_stack = card.stack

            if session.get('score') == 1:
                card.stack = max([card.stack, 0]) + 1
                card.last_correct = datetime.datetime.now()
            else:
                card.stack = 0

            flash("Moved from stack " + str(old_stack) + " to " + str(card.stack))

    elif action == 'next':
        buffer = session.get('card_buffer')

        if session.get('score') < 1:
            buffer.append(session['active_card'])

        if len(buffer) > 0:
            session['active_card'] = buffer.pop(0)
            card = Card.query.filter_by(id=session.get('active_card')).first()
            session['card_buffer'] = buffer
            session['score'] = -1
        else:
            card = create_card_session()

        db.session.commit()

    return render_template('learn.html',
        wordFront=card.front,
        answerUser=answerUser,
        answerSystem=answerSystem,
        score=round(score, 2))


@main.route('/media-learn/', methods=['GET', 'POST'])
def media_learn():
    action = request.form.get('action', "none")
    answer = request.form.get('answer', "")
    topic = request.args.get('topic', None)

    if not session.get('active_card') or topic:
        card = create_card_session(topic)
    else:
        card = Card.query.filter_by(id=session.get('active_card')).first()

    # no cards available
    if not card:
        flash("Session is complete.")
        return redirect(url_for('.page_main'))

    answerUser, answerSystem = '', ''

    wspan = '<span class="{modifier}">{word}</span>'

    # The score takes values between 0 and 1.
    # 0 means no letters are correct, and 1 means all letters are correct.
    # Formula: score = correct letters / (correct + missing + incorrect letters)
    score = 0.0

    if action == 'review':
        # assess action and create result parts
        if not answer:
            answer = ''
        if card.back == answer:
            answer = answer + " &#10004;"
            answerUser = wspan.format(modifier='correct', word=answer)
            score = 1.0
        else:
            answerUser, nCorrect, nWrong1 = getCorrection(answer, card.back, 'excessive', wspan)
            answerSystem, nCorrect, nWrong2 = getCorrection(answer, card.back, 'missing', wspan)
            score = nCorrect / (nCorrect + nWrong1 + nWrong2)
            store_action(card.id, score)

        if session.get('score') < 0:
            session['score'] = score

    elif action == 'next':
        buffer = session.get('card_buffer')

        if session.get('score') < 1:
            buffer.append(session['active_card'])

        if len(buffer) > 0:
            session['active_card'] = buffer.pop(0)
            card = Card.query.filter_by(id=session.get('active_card')).first()
            session['card_buffer'] = buffer
            session['score'] = -1
        else:
            card = create_card_session()

        db.session.commit()

    return render_template('media-learn.html',
                           audio=card.back,
                           answerUser=answerUser,
                           answerSystem=answerSystem,
                           score=round(score, 2))